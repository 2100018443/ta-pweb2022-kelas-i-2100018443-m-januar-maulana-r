<?php
	$nilai = 50;
	if ($nilai >= 60){
		echo "Nilai Anda $nilai, Anda LULUS";
	}else{
		echo "Nilai Anda $nilai, Anda GAGAL";
	}
	echo "<br><br>SETELAH DI MODIFIKASI";
	echo "<hr><hr>";
?>

<html>
    <head>
        <title>TUGAS PWEB 10</title>
    </head>
    <body>
        <form id="form1" name="form1" method="post" action="">
			<p>PETUNJUK MEMASUKKAN NILAI : 0 - 4.00</p>
            <table>
                <tr>
                    <td>
                        Masukan Nilai Anda
                    </td>
                    <td>
                        : <input type="text" name="nilai" id="nilai">
                    </td>
                </tr>
                  <td colspan="2">
                        <input type="submit" name="submit" value="Kirim Data" id="submit">
                    </td>
                </tr>
            </table>
        </form>

<?php
	$nilai = $_POST['nilai'];
	$nilaihuruf = $_POST['nilai'];
	$submit = $_POST['submit'];


	if(($submit < 0) || ($nilai > 4.00)){
		echo "Nilai yang anda masukkan salah!!!";
	}
	else{
		if (($nilai >= 3.68) && ($nilai <= 4.00)){
			$nilaihuruf = "A";
		}
		else if (($nilai >= 3.34) && ($nilai <= 3.67)){
			$nilaihuruf = "A-";
		}
		else if (($nilai >= 3.01) && ($nilai <= 3.33)){
			$nilaihuruf = "B+";
		}
		else if (($nilai >= 2.68) && ($nilai <= 3.00)){
			$nilaihuruf = "B";
		}
		else if (($nilai >= 2.34) && ($nilai <= 2.67)){
			$nilaihuruf = "B-";
		}
		else if (($nilai >= 2.01) && ($nilai <= 2.33)){
			$nilaihuruf = "C+";
		}
		else if (($nilai >= 1.68) && ($nilai <= 2.00)){
			$nilaihuruf = "C";
		}
		else if (($nilai >= 1.34) && ($nilai <= 1.67)){
			$nilaihuruf = "C-";
		}
		else if (($nilai >= 1.01) && ($nilai <= 1.33)){
			$nilaihuruf = "D+";
		}
		else if (($nilai >= 0.01) && ($nilai <= 1.00)){
			$nilaihuruf = "D";
		}
		else if (($nilai >= 0.00) && ($nilai < 0.01)){
			$nilaihuruf = "E";
		}
		echo "======= NILAI =======<br>";
		echo "Nilai Angka   : $nilai   <br>";
		echo "Nilai Huruf : $nilaihuruf <br>";
		echo "=====================<br>";
	}
?>
</body>
</html>